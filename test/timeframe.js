import Timeframe from "../src/utils/timeframe";
let expect = require("chai").expect;

function objWrapper(str) {
  return () => new Timeframe(str);
}

const invalidList = [
  "31 12 2014",
  "1a 12 2014, 11 03 2016",
];

const validList = [
  "11 12 2016, 31 12 2016",
  "11 12 2016, 11 03 2014",
  "11 12 2016, 11 12 2016",
];


describe("Timeframe class", () => {

  describe("with invalid value", () => {
    it("should throw error if parse timeframe in invalid format: " + invalidList[0], () => {
      expect(objWrapper(invalidList[0])).to.throw(/Please use the right format 'DD MM YYYY, DD MM YYYY/);
    });

    it("should throw error if parse timeframe has invalid date: " + invalidList[1], () => {
      expect(objWrapper(invalidList[1])).to.throw(/Invalid date/);
    });

  });

  describe("with valid value", () => {
    it("should split dates into frames and compute difference from: " + validList[0], () => {
      let timeframe = new Timeframe(validList[0]);
      expect(timeframe.frames.length).to.equal(2);
      expect(timeframe.frames[0].dateString).to.equal('11 12 2016');
      expect(timeframe.frames[1].dateString).to.equal('31 12 2016');
      expect(timeframe.difference).to.equal(20);
    });

    it("should split dates into frames, sort it and compute difference from: " + validList[1], () => {
      let timeframe = new Timeframe(validList[1]);
      expect(timeframe.frames.length).to.equal(2);
      expect(timeframe.frames[0].dateString).to.equal('11 03 2014');
      expect(timeframe.frames[1].dateString).to.equal('11 12 2016');
      expect(timeframe.difference).to.equal(1005);
    });

    it('should print difference in the right format from: ' + validList[1], () => {
      let timeframe = new Timeframe(validList[1]);
      expect(timeframe.print()).to.equal("11 03 2014, 11 12 2016, 1005 days");
    });

    it('should print difference in 0 day from: ' + validList[2], () => {
      let timeframe = new Timeframe(validList[2]);
      expect(timeframe.print()).to.equal("11 12 2016, 11 12 2016, 0 day");
    });

  });
});
