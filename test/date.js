import NaiveDate from "../src/utils/date";
let expect = require("chai").expect;

function objWrapper(date) {
  return () => new NaiveDate(date);
}

const invalidList = [
  "1a 12 2014",
  "00 12 0000",
  "32 12 0001",
  "31 13 0001",
  "31 12 199",
  "31 11 1999",
];

const validList = [
  "01 02 2017",
  "01 01 0001",
  "28 02 2017",
  "01 03 2016",
];

describe("Date class", () => {

  describe("with invalid value", () => {
    it("should throw error if date in invalid format: " + invalidList[0], () => {
      expect(objWrapper(invalidList[0])).to.throw(/Invalid date/);
    });

    it("should throw error if date is wrong: " + invalidList[1], () => {
      expect(objWrapper(invalidList[1])).to.throw(/Invalid date/);
    });

    it("should throw error if day of month is wrong: " + invalidList[2], () => {
      expect(objWrapper(invalidList[2])).to.throw(/Invalid date/);
    });

    it("should throw error if month is wrong: " + invalidList[3], () => {
      expect(objWrapper(invalidList[3])).to.throw(/Invalid date/);
    });

    it("should throw error if year is wrong: " + invalidList[4], () => {
      expect(objWrapper(invalidList[4])).to.throw(/Invalid date/);
    });

    it("should throw error if day if more than maximum day in month: " + invalidList[5], () => {
      expect(objWrapper(invalidList[5])).to.throw(/Invalid day in month/);
    });
  });

  describe("with valid date", () => {
    it("should parse date into day, month, year and daysAfterAC: " + validList[0], () => {
      let date = new NaiveDate(validList[0]);
      expect(date.day).to.equal(1);
      expect(date.month).to.equal(2);
      expect(date.year).to.equal(2017);
      expect(date.daysAfterAC).to.equal(736357);
    });

    it("should parse date from beginning: " + validList[1], () => {
      let date = new NaiveDate(validList[1]);
      expect(date.day).to.equal(1);
      expect(date.month).to.equal(1);
      expect(date.year).to.equal(1);
      expect(date.daysAfterAC).to.equal(1);
    });

    it("should compare and return value between two dates: " + validList[2] + " - " + validList[3], () => {
      let date1 = new NaiveDate(validList[2]);
      let date2 = new NaiveDate(validList[3]);
      expect(date1.isAfter(date2)).to.be.true;
    });

    it('should get difference between two days: ' + validList[2] + " - " + validList[3], () => {
      let date1 = new NaiveDate(validList[2]);
      let date2 = new NaiveDate(validList[3]);
      expect(date1.getDifference(date2)).to.equal(363);
    });

  });
});
