# TimeFrame

## What it contains?

* Two utilities classes which helps to compute day difference between two dates in DD MM YYYY format without using Date builtin
* Timeframe class takes care of checking input format, parsing with NaiveDate class, sort and compute difference in days and return "DD MM YYYY, DD MM YYYY, difference" with print function
* NaiveDate implements date parsing with functions to compare between dates

## Dependencies

* [node.js](https://nodejs.org/) with [npm](https://www.npmjs.com/)

## Install

Install JavaScript dependencies:

```
$ npm i
```

## Test

Run Mocha test

```
$ npm test
```

## License
Copyright Reserve
