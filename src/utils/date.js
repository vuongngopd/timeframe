export default class NaiveDate {
  /*
    Date class to create date object from date string with DD MM YYYY format
    Parse string into object which can be compared
    Throw error if date string is invalid
    Properties:
      dateString <string>
      day <int>
      month <int>
      year <int>
      daysAfterAC <int>
    Functions:
      isAfter(<NaiveDate>)
      getDifference(<NaiveDate>)
   */
  constructor(str) {
    this.dateString = str;
    this.getDaysFromACToNow = this.getDaysFromACToNow.bind(this);
    this.getDaysCurrentYear = this.getDaysCurrentYear.bind(this);
    this.getDaysFromAC = this.getDaysFromAC.bind(this);
    //Initialize object by validiating and parse date string
    try {
      this.check.bind(this)();
      this.parse.bind(this)();
    } catch(err) {
      throw err;
    }
  }

  /*
   Loosely validate date string with regex
   */
  check() {
    var date_regex = /^(0[1-9]|1\d|2\d|3[01])[- /.](0[1-9]|1[0-2])[- /.]\d{4}$/ ;
    if (!date_regex.test(this.dateString)) {
      throw new Error('Invalid date');
    };
  }

  /*
   Parse date into day, month, year and daysAfterAC
   */
  parse() {
    let re = /[- /.]/;
    let dateValues = this.dateString.split(re);
    this.day = parseInt(dateValues[0]);
    this.month = parseInt(dateValues[1]);
    this.year = parseInt(dateValues[2]);
    this.validateDay.bind(this)(); //Validate day in month as not tested in regex
    this.daysAfterAC = this.getDaysFromACToNow(); //Convert date string into comparable number of days
  }

  /*
   Get total AC days
   Make it simple for comparing dates
   Take into account leap year
   */
  getDaysFromACToNow() {
    let currentYearDays = this.getDaysCurrentYear();
    let previousYearDays = this.getDaysFromAC(this.year -1);
    return currentYearDays + previousYearDays;
  }

  /*
   Get total days this year
   Take into account leap year
   */
  getDaysCurrentYear() {
    let totalDays = this.day;
    const leapYear = this.isLeapYear(this.year);
    for (let month = 1; month < this.month; month++) {
      let maxDayInMonth = this.getDaysInMonth(month, leapYear);
      totalDays += maxDayInMonth;
    }
    return totalDays;
  }

  /*
   Get total days from AC to last year
   */
  getDaysFromAC(year) {
    const totalLeapYears = Math.floor(year / 4 - year / 100 + year / 1000);
    return year* 365 + totalLeapYears;
  }

  /*
   */
  isAfter(date) {
    if (this.daysAfterAC > date.daysAfterAC) {
      return true;
    }
    return false;
  }

  /*
   Get days difference from two dates
   */
  getDifference(date) {
    return Math.abs(this.daysAfterAC - date.daysAfterAC);
  }

  /*
   Helper for validating maximum number of days in month
   */
  validateDay() {
    const maxDayInMonth = this.getDaysInMonth(this.month, this.isLeapYear(this.year));
    if (this.day > maxDayInMonth) {
      throw new Error('Invalid day in month');
    }
  }


  /*
   Helper to check leap year
   */
  isLeapYear(year) {
    if (year % 4 != 0) {
      return false;
    } else if (year % 100 == 0) {
      return true;
    } else if (year % 400 == 0) {
      return false;
    } else {
      return true;
    }
  }

  /*
  Helper to get maximum number of days in month
   */
  getDaysInMonth(month, isLeapYear) {
    const longMonths = [1, 3, 7, 8, 10, 12];
    if (month === 2 && isLeapYear) {
      return 29;
    } else if (month === 2) {
      return 28;
    } else if (longMonths.findIndex(x => x === month) > -1 ){
      return 31;
    } else {
      return 30;
    }
  }

}
