import NaiveDate from './date';

export default class Timeframe {
  /*
    Timeframe class create object includes start and end times from DD MM YYYY, DD MM YYYY
    Parse string into NaiveDates in increasing order and compute difference between dates.
    Properties:
      originStr <string>
      frames <[NaiveDate]>
    Functions:
      print()
  */
  constructor(str) {
    this.originStr = str;
    this.frames = [];
    this.print = this.print.bind(this);
    //Initialize validation, parsing, sorting and calculation
    try {
      this.parse.bind(this)();
      this.sort.bind(this)();
      this.getDifference.bind(this)();
    } catch(err) {
      throw err;
    }
  }

  /*
   Split original string into dates, validate and parse into NaiveDate
   */
  parse() {
    const dates = this.originStr.split(',');
    if (dates.length != 2) {
      throw new Error("Please use the right format 'DD MM YYYY, DD MM YYYY");
    }
    dates.forEach((d, i) => {
      try {
        let date = new NaiveDate(d.trim()); //Throw error if date string is wrong
        this.frames.push(date); //Push NaiveDate object to frames unsorted
      } catch(err) {
        throw err;
      }
    });
  }

  /*
   Sort where the first date is the earliest, the second date is the latest
   */
  sort() {
    this.frames = this.frames.sort((a, b) => a.isAfter(b));
  }

  /*
   Use sorted dates to compute difference in days
   */
  getDifference() {
    this.difference = this.frames[0].getDifference(this.frames[1]);
  }

  /*
   Return with 'DD MM YYYY, DD MM YYYY, difference' as required
   */
  print() {
    return `${this.frames[0].dateString}, ${this.frames[1].dateString}, ${this.difference} ${this.difference < 2 ? 'day' : 'days'}`;
  }
}
